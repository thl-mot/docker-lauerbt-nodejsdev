#!/bin/bash

echo "root.sh => install packages"
apt-get -qq update
apt-get install -y git wget xterm ant unzip tar curl

curl -sL https://deb.nodesource.com/setup_6.x | bash -
apt-get install -y nodejs

apt-get clean
apt-get purge


npm install gulp-cli -g
npm install gulp -D
touch gulpfile.js

rm -r /opt/customisation


